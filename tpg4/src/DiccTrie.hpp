#include "DiccTrie.h"

template <typename T>
DiccTrie<T>::DiccTrie() : raiz(new Nodo()), _claves(), _size(0) { }

template <typename T>
DiccTrie<T>::DiccTrie(const DiccTrie<T>& copy) : DiccTrie() { *this = copy; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
DiccTrie<T>& DiccTrie<T>::operator=(const DiccTrie<T>& sm) {
    delete raiz;
    raiz = new Nodo(*sm.raiz);
    _size = sm._size;
    _claves = sm._claves;
}

template <typename T>
DiccTrie<T>::~DiccTrie() {
    delete raiz;
}

template<typename T>
void DiccTrie<T>::insert(const pair<string, T>& kv) {
    Nodo* nodo = raiz;
    for (char c : kv.first) {
        if (!nodo->siguientes[c])
            nodo->siguientes[c] = new Nodo();
        nodo = nodo->siguientes[c];
    }
    if (nodo->definido())
        delete (nodo->definicion);
    else {
        _size++;
        _claves.push_back(kv.first);
    }
    nodo->definicion = new T(kv.second);
}

template <typename T>
T& DiccTrie<T>::operator[](const string &key){
    if (!count(key))
        insert(make_pair(key, T()));
    return at(key);
}

template <typename T>
int DiccTrie<T>::count(const string& key) const{
    Nodo* nodo = raiz;
    for (char c : key) {
        if (!nodo->siguientes[c])
            return false;
        nodo = nodo->siguientes[c];
    }
    return nodo->definido();
}

template <typename T>
const T& DiccTrie<T>::at(const string& key) const {
    Nodo* nodo = raiz;
    for (char c : key)
        nodo = nodo->siguientes[c];

    return nodo->definicion;
}

template <typename T>
T& DiccTrie<T>::at(const string& key) {
    Nodo* nodo = raiz;
    for (char c : key)
        nodo = nodo->siguientes[c];

    return *(nodo->definicion);
}

template <typename T>
void DiccTrie<T>::erase(const string& key) {
    Nodo* nodo = raiz;
    Nodo* erase_hat = raiz;
    char  erase_path = key[0];

    for (char c : key) {
        if (nodo->hijos() > 1 || nodo->definido()) {
            erase_hat = nodo;
            erase_path = c;
        }
        nodo = nodo->siguientes[c];
    }

    if (nodo->hijos()) {
        delete (nodo->definicion);
        nodo->definicion = nullptr;
    }
    else {
        delete(erase_hat->siguientes[erase_path]);
        erase_hat->siguientes[erase_path] = nullptr;
    }
    _size--;
}

template <typename T>
int DiccTrie<T>::size() const{
    return _size;
}

template <typename T>
bool DiccTrie<T>::empty() const{
    return  !_size;
}

template<typename T>
list<Jugador> DiccTrie<T>::claves() const {
    return _claves;
}
