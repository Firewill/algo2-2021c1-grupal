#include "Mapa.h"
#define at(x) (*this)[x]
Mapa::Mapa(const Nat largo, const Nat alto, const Coordenada inicio, const Coordenada llegada,
           const set <Coordenada>& paredes, const set <Coordenada>& fantasmas, const set <Coordenada>& chocolates)
        : _largo(largo), _alto(alto), _inicio(inicio), _llegada(llegada) {

    _elementos = vector<Elemento>(largo * alto, VACIO);

    for (auto p : paredes)
        at(p) = PARED;

    for(auto f : fantasmas)
        at(f) = FANTASMA;

    for(auto c : chocolates)
        at(c) = CHOCOLATE;
}

Elemento &Mapa::operator[](Coordenada c) {
    return _elementos[(c.second - 1) * _largo + (c.first - 1)];
}

const Elemento &Mapa::operator[](Coordenada c) const {
    return _elementos[(c.second - 1) * _largo + (c.first - 1)];
}

Nat Mapa::largo() const {
    return _largo;
}

Nat Mapa::alto() const {
    return _alto;
}

Coordenada Mapa::inicio() const {
    return _inicio;
}

Coordenada Mapa::llegada() const {
    return _llegada;
}
#undef at