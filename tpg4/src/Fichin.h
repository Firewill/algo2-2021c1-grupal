#ifndef FICHIN_H
#define FICHIN_H

#include "Partida.h"
#include "DiccTrie.h"
using Ranking = DiccTrie<Nat>;

class Fichin {
public:
    explicit Fichin(const Mapa& mapa);

    void empezarPartida(Jugador jugador);

    void moverFichin(Direccion direccion);

    Ranking ranking() const;

    pair<Jugador, Nat> objetivo() const;

    //GTest
    Partida partida() const;
    bool enPartida() const;
    string jugador() const;

private:
    bool    _enPartida;
    Jugador _jugador;
    Ranking _ranking;
    Partida _partida;
};

#endif //FICHIN_H
