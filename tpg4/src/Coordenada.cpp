#include "Coordenada.h"

Coordenada operator + (const Coordenada& a, const Coordenada& b) {
    return {a.first + b.first, a.second + b.second};
}

Coordenada operator += (Coordenada& a, const Coordenada& b) {
    a = {a.first + b.first, a.second + b.second};
    return a;
}

Coordenada operator - (const Coordenada& v) {
    return {-v.first, -v.second};
}

Coordenada operator - (const Coordenada& a, const Coordenada& b) {
    return {a.first - b.first, a.second - b.second};
}

Coordenada operator -= (Coordenada& a, const Coordenada& b) {
    a = {a.first - b.first, a.second - b.second};
    return a;
}

bool operator < (const Coordenada& a, const Coordenada& b) {
    return a.first < b.first && a.second < b.second;
}