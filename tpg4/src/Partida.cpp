#include "Partida.h"

const Nat radioFantasmas = 3;

Partida::Partida(const Mapa& mapa) : _puntaje(0), _mapa(mapa) {
    for (int x = 1; x <= _mapa.largo(); x++)
        for (int y = 1; y <= _mapa.alto(); y++)
            if (_mapa[{x, y}] == CHOCOLATE)
                _chocolates.push_back({x, y});
    _pacman = {_mapa.inicio(), 0};
}

void Partida::iniciarPartida() {
    for(auto c : _chocolates)
        _mapa[c] = CHOCOLATE;
    _pacman.posicion = _mapa.inicio();
    _pacman.inmunidad = (_mapa[_mapa.inicio()] == CHOCOLATE) * 10;
    _mapa[_mapa.inicio()] = VACIO;
    _puntaje = 0;
}

bool Partida::gano() const {
    return _pacman.posicion == _mapa.llegada();
}

bool Partida::perdio() const {
    bool res = false;
    Nat distancia = 3;
    Coordenada v = _pacman.posicion;

    Rango filas = {v.first - (distancia < v.first - 1 ? distancia : v.first - 1),
                   min(v.first + distancia, _mapa.largo())};
    for (int i = filas.first; i <= filas.second; i++) {
        Nat restante = distancia - (i < v.first ? v.first - i : i - v.first); //y = d − abs(v.x − i)
        Rango columnas = {v.second - (restante < v.second - 1 ? restante : v.second - 1),
                          min(v.second + restante, _mapa.alto())};
        for (int j = columnas.first; j <= columnas.second; j++)
            if (_mapa[{i, j}] == FANTASMA && _pacman.inmunidad == 0)
                res = true;
    }

    return res && !gano();
}

bool Partida::moverJugador(Direccion direccion){
    Coordenada nuevaPosicion = _pacman.posicion;

    switch (direccion) {
        case ARRIBA:
            if(_pacman.posicion.second < _mapa.alto())
                nuevaPosicion += Coordenada(0, 1);
            break;
        case ABAJO:
            if(_pacman.posicion.second > 1)
                nuevaPosicion -= Coordenada(0, 1);
            break;
        case IZQUIERDA:
            if(_pacman.posicion.first > 1)
                nuevaPosicion -= Coordenada(1, 0);
            break;
        case DERECHA:
            if(_pacman.posicion.first < _mapa.largo())
                nuevaPosicion += Coordenada(1, 0);
            break;
    }

    if (_pacman.posicion != nuevaPosicion && _mapa[nuevaPosicion] != PARED) {
        _pacman = {nuevaPosicion, _pacman.inmunidad != 0 ? --_pacman.inmunidad : 0};
        _puntaje++;
    }

    if (_mapa[_pacman.posicion] == CHOCOLATE) {
        _pacman.inmunidad = 10;
        _mapa[_pacman.posicion] = VACIO;
    }

    return gano() || perdio();
}

Coordenada Partida::posicionJugador() const {
    return _pacman.posicion;
}

Nat Partida::puntaje() const {
    return _puntaje;
}

Nat Partida::inmunidad() const {
    return _pacman.inmunidad;
}

Mapa Partida::mapa() const {
    return _mapa;
}

vector<Coordenada> Partida::chocolatesIniciales() const {
    return _chocolates;
}
