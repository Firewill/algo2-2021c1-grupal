Interfaz:
	Se explica con:	Mapa
	Géneros: mapa
	
	Coordenada ES tupla ⟨nat, nat⟩
	
	Operaciones básicas de mapa:
		NuevoMapa(in largo, alto: nat, in inicio, llegada: coordenada, in paredes,fantasmas, chocolates: conj(coordenada)) ↦ res: mapa
		• Pre ≡ { inicio ≠ llegada ∧ todosEnRango(paredes ∪ fantasmas ∪ chocolates ∪ {inicio,llegada},largo,alto) ∧
																 disjuntosDeAPares(paredes,fantasmas,chocolates) }
		• Post ≡ { res =obs NuevoMapa(largo,alto,inicio,llegada,paredes,fantasmas,chocolates) }
		• Complejidad: Θ(1)
		• Descripción: Crea un mapa de dimension alto×largo, con las cordenadas de inicio y llegada que pasemos por parametro y tambien las paredes, fantasmas y chocolates.
		• Aliasing: No presenta aspectos de aliasing.
		---
		Largo(in m: mapa) ↦ res: nat
		• Pre ≡ {true}
		• Post ≡ {res =obs largo(m)}
		• Complejidad: Θ(1)
		• Descripción: Dado un mapa devuelve su largo.
		• Aliasing: No presenta aspectos de aliasing.
		---		
		Alto(in m: mapa) ↦ res: nat
		• Pre ≡ {true}
		• Post ≡ {res =obs alto(m)}
		• Complejidad: Θ(1)
		• Descripción: Dado un mapa devuelve su alto.
		• Aliasing: No presenta aspectos de aliasing.
		---
		Inicio(in m: mapa) ↦ res: coordenada
		• Pre ≡ {true}
		• Post ≡ {res =obs inicio(m)}
		• Complejidad: Θ(1)
		• Descripción: Dado un Mapa devuelve la posicion de inicio.
		• Aliasing: No presenta aspectos de aliasing.
		---
		Llegada(in m: mapa) ↦ res: coordenada
		• Pre ≡ {true}
		• Post ≡ {res =obs llegada(m)}
		• Complejidad: Θ(1)
		• Descripción: Dado un mapa devuelve la posicion de llegada
		• Aliasing: No presenta aspectos de aliasing.
		---
		Paredes(in m: mapa) ↦ res: conj(coordenada)
		• Pre ≡ {true}
		• Post ≡ {res =obs paredes(m)}
		• Complejidad: Θ(1)
		• Descripción: Dado un mapa devuelve un puntero al conjunto de coordenadas que contiene paredes.
		• Aliasing: el puntero es aliasing.
		---
		Fantasmas(in m: mapa) ↦ res: conj(coordenada)
		• Pre ≡ {true}
		• Post ≡ {res =obs fantasmas(m)}
		• Complejidad: Θ(1)
		• Descripción: Dado un mapa devuelve un puntero a conjunto de coordenadas que contiene fantasmas.
		• Aliasing: el puntero es aliasing.
		---
		Chocolates(in m: mapa) ↦ res: conj(coordenada)
		• Pre ≡ {true}
		• Post ≡ {res =obs chocolates(m)}
		• Complejidad: Θ(1)
		• Descripción: Dado un mapa devuelve un puntero a conjunto de coordenadas que contiene chocolates.
		• Aliasing: el puntero es aliasing.
		---
		Distancia(in c1: coordenada, in c2: coordenada) ↦ res: nat
		• Pre ≡ {true}
		• Post ≡ {res =obs distancia(c1, c2)}
		• Complejidad: Θ(1)
		• Descripción: Dadas dos coordenas devuelve la distancia manhattan entre ellas.
		• Aliasing: No presenta aspectos de aliasing.
		---
		Representación:
			conj es el modulo conjuntoLineal
			
			mapa se representa con estr donde 
				estr es tupla ⟨	largo: nat, alto: nat, valores: matriz, chocolates: conj(Coordena) ⟩
												
			donde
			coordenada es nat
			matriz es vector(relleno)
			relleno es ENUM(INICIO, LLEGADA, VACIO, PARED, FANTASMA, CHOCOLATE)
		---
		Invariante de Representación:
			Rep:	estr	->	boolean
			(∀e: estr) Rep(e)	≡ true ⇐⇒ (1)∧(2)∧(3)∧(4)
				donde:
				(1) ≡ incio y llegada no pueden ser la misma coordenada.
				(2)	≡	inicio, llegada, toda pared, todo fantasma y todo chocolate está dentro de las dimensiones delimitadas por largo y alto
				(3)	≡	sobre el inicio y la llegada no hay ni una pared ni un fantasma ni un chocolate
				(4)	≡	no hay intersección entre los conjuntos paredes, fantasmas y chocolates
		---
		Función de abstracción:
			Abs:	estr	->	mapa	{Rep(e)}
			(∀e: estr) Abs(e) =obs m: mapa | (1)∧(2)∧(3)∧(4)∧(5)∧(6)∧(7)
				donde: 
				(1)	≡	largo(m)      =obs e.largo
				(2)	≡	alto(m)       =obs e.alto
				(3) ≡ inicio(m)     =obs e.inicio
				(4) ≡ llegada(m)    =obs e.llegada
				(5) ≡ paredes(m)    =obs e.paredes
				(6) ≡ fantasmas(m)  =obs e.fantasmas
				(7) ≡ chocolates(m) =obs e.chocolates
		---
	Algoritmos del módulo:
		iNuevoMapa(in largo,alto:nat, in inicio,llegada:coordenada, in paredes,fantasmas,chocolates:conj(coordenada) ) -> res: mapa
			res ← ⟨largo, alto, incio, llegada, paredes, fantasmas, chocolates⟩
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)
	  ---
		iLargo(in m: mapa) -> res: nat
			res ← mapa.largo
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)
		---
		iAlto(in m: mapa) -> res: nat
			res ← mapa.alto
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)
		---
		iInicio(in m: mapa) -> res: coordenada
			res ← mapa.inicio
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)
	  ---
		iLlegada(in m: mapa) -> res: coordenada
			res ← mapa.llegada
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)
		---
		iParedes(in m: mapa) -> res: conj(coordenada)
			res ← mapa.paredes
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)
		---
		iFantasmas(in m: mapa) -> res: coordenada
			res ← mapa.fantasmas
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)
		---
		iChocolates(in m: mapa) -> res: coordenada
			res ← mapa.chocolates
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)
		---
		iDistancia(in m: mapa) -> res: coordenada
			res ← mapa.chocolates
			Complejidad Θ(1) Es un acceso y una asignacion: Θ(1) + Θ(1) = Θ(1)			
		---
