#ifndef MAPA_H
#define MAPA_H

#include "Coordenada.h"

enum Elemento {
    VACIO, CHOCOLATE, PARED, FANTASMA
};

class Mapa {
public:
    Mapa(Nat largo, Nat alto, Coordenada inicio, Coordenada llegada,
         const set<Coordenada>& paredes, const set<Coordenada>& fantasmas, const set<Coordenada>& chocolates);

    Elemento& operator[](Coordenada c);

    const Elemento& operator[](Coordenada c) const;

    Nat largo() const;
    Nat alto() const;
    // -- -- -- -- --
    Coordenada inicio() const;
    Coordenada llegada() const;

private:
    Nat _largo, _alto;
    Coordenada _inicio, _llegada;

    vector<Elemento> _elementos;
};

#endif //MAPA_H