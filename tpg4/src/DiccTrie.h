#ifndef DICCTRIE_H_
#define DICCTRIE_H_

#include <string>
#include <vector>
#include <list>
#include "Fichin.h"

using namespace std;

template<typename T>
class DiccTrie {
public:
    DiccTrie();

    DiccTrie(const DiccTrie<T>& copy);

    DiccTrie& operator = (const DiccTrie& sm);

    ~DiccTrie();

    void insert(const pair<string, T>& kv);

    int count(const string& key) const;

    const T& at(const string& key) const;
    T& at(const string& key);

    void erase(const string& key);

    int size() const;

    bool empty() const;

    list<Jugador> claves() const;

    T &operator[](const string &key);

private:

    struct Nodo {
        vector<Nodo*> siguientes;
        T* definicion;

        Nodo() : siguientes(256, nullptr), definicion(nullptr) { };
        explicit Nodo(T* def) : siguientes(256, nullptr), definicion(def) { };

        Nodo(const Nodo &n) : siguientes(vector<Nodo*>(256)), definicion(n.definido() ? new T(*n.definicion) : nullptr) {
            for (int i = 0; i < 256; i++)
                if (n.siguientes[i])
                    siguientes[i] = new Nodo(*n.siguientes[i]);
        };

        ~Nodo() {
            for (int i = 0; i < 256; i++)
                delete siguientes[i];
            delete definicion;
        }

        bool definido() const { return definicion != nullptr; }

        int hijos() const {
            int h = 0;
            for (Nodo* n : siguientes)
                if(n)
                    h++;
            return h;
        }
    };

    Nodo* raiz;
    int _size;
    list<Jugador> _claves;
};

#include "DiccTrie.hpp"
#endif // DICCTRIE_H_
