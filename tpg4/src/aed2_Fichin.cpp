#include "aed2_Fichin.h"
aed2_Fichin::aed2_Fichin(Nat largo, Nat alto, Coordenada inicio, Coordenada llegada,
                         set<Coordenada> paredes, set<Coordenada> fantasmas, set<Coordenada> chocolates) :
                                _fichin(Fichin(Mapa(largo, alto, inicio, llegada,
                                                    paredes, fantasmas, chocolates))) { }

void aed2_Fichin::nuevaPartida(Jugador jugador) {
    _fichin.empezarPartida(jugador);
}

ResultadoMovimiento aed2_Fichin::mover(Direccion dir) {
    _fichin.moverFichin(dir);
    if (_fichin.partida().perdio())
        return PERDIO;
    else if (_fichin.partida().gano())
        return GANO;
    return SIGUE;
}

bool aed2_Fichin::alguienJugando() const {
    return _fichin.enPartida();
}

Jugador aed2_Fichin::jugadorActual() const {
    return _fichin.jugador();
}

map<Jugador, Puntaje> aed2_Fichin::ranking() const {
    map<Jugador, Puntaje> m;
    list<Jugador> js = _fichin.ranking().claves();
    int size = _fichin.ranking().claves().size();
    auto it = js.begin();
    for (int i = 0; i < size; i++)
        m.insert({*it, _fichin.ranking()[*it++]});
    return m;
}

pair<Jugador, Puntaje> aed2_Fichin::objetivo() const {
    return _fichin.objetivo();
}

Coordenada aed2_Fichin::jugador() const {
    return _fichin.partida().posicionJugador();
}

Nat aed2_Fichin::cantidadMovimientos() const {
    return _fichin.partida().puntaje();
}

Nat aed2_Fichin::inmunidad() const {
    return _fichin.partida().inmunidad();
}

Nat aed2_Fichin::largo() const {
    return _fichin.partida().mapa().largo();
}

Nat aed2_Fichin::alto() const {
    return _fichin.partida().mapa().alto();
}

Coordenada aed2_Fichin::inicio() const {
    return _fichin.partida().mapa().inicio();
}

Coordenada aed2_Fichin::llegada() const {
    return _fichin.partida().mapa().llegada();
}

set<Coordenada> aed2_Fichin::paredes() const {
    set<Coordenada> p;
    for (int x = 1; x <= _fichin.partida().mapa().largo(); x++)
        for (int y = 1; y <= _fichin.partida().mapa().alto(); y++)
            if (_fichin.partida().mapa()[{x, y}] == PARED)
                p.insert({x, y});

    return p;
}

set<Coordenada> aed2_Fichin::fantasmas() const {
    set<Coordenada> f;
    for (int x = 1; x <= _fichin.partida().mapa().largo(); x++)
        for (int y = 1; y <= _fichin.partida().mapa().alto(); y++)
            if (_fichin.partida().mapa()[{x, y}] == FANTASMA)
                f.insert({x, y});

    return f;
}

set<Coordenada> aed2_Fichin::chocolatesActuales() const {
    set<Coordenada> c;
    for (int x = 1; x <= _fichin.partida().mapa().largo(); x++)
        for (int y = 1; y <= _fichin.partida().mapa().alto(); y++)
            if (_fichin.partida().mapa()[{x, y}] == CHOCOLATE)
                c.insert(Coordenada(x, y));

    return c;
}

set<Coordenada> aed2_Fichin::chocolatesIniciales() const {
    set<Coordenada> s = set<Coordenada>();
    for (int i = 0; i < _fichin.partida().chocolatesIniciales().size(); i++)
        s.insert(_fichin.partida().chocolatesIniciales()[i]);
    return s;
}
