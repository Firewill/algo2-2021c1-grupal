#ifndef COORDENADA_H
#define COORDENADA_H

#include "Tipos.h"

using Coordenada = pair<Nat, Nat>;
using Rango = pair<Nat, Nat>;

Coordenada operator +  (const Coordenada&, const Coordenada&);
Coordenada operator += (Coordenada&, const Coordenada&);
Coordenada operator -  (const Coordenada&);
Coordenada operator -  (const Coordenada&, const Coordenada&);
Coordenada operator -= (Coordenada&, const Coordenada&);
bool       operator <  (const Coordenada&, const Coordenada&);
#endif //COORDENADA_H