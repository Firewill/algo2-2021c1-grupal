#ifndef PARTIDA_H
#define PARTIDA_H

#include "Mapa.h"

class Partida {
public:
    explicit Partida(const Mapa& mapa);

    void iniciarPartida();

    bool gano() const;

    bool perdio() const;

    bool moverJugador(Direccion direccion);

    //GTest
    Coordenada posicionJugador() const;
    Nat puntaje() const;
    Nat inmunidad() const;
    Mapa mapa() const;
    vector<Coordenada> chocolatesIniciales() const;

private:
    struct Pacman {
        Coordenada posicion;
        Nat inmunidad;
    };

    Nat _puntaje;
    Pacman _pacman;
    vector<Coordenada> _chocolates;
    Mapa _mapa;
};

#endif //PARTIDA_H
