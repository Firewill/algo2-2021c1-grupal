#include "Fichin.h"

Fichin::Fichin(const Mapa& mapa) : _enPartida(false), _jugador(), _ranking(), _partida(Partida(mapa)) { }

void Fichin::empezarPartida(Jugador jugador) {
    _partida.iniciarPartida();
    _jugador = jugador;
    _enPartida = !_partida.perdio();
}

void Fichin::moverFichin(Direccion direccion) {
    if (_partida.moverJugador(direccion)) {     //moverJugador devuelve true sii terminó la partida (ganó o perdió)
        if (_partida.gano())
            _ranking.insert({_jugador, _ranking.count(_jugador) ? min(_partida.puntaje(), ranking()[_jugador]) : _partida.puntaje() });
        _enPartida = false;
    }
}

Ranking Fichin::ranking() const {
    return _ranking;
};

Partida Fichin::partida() const {
    return _partida;
}

bool Fichin::enPartida() const {
    return _enPartida;
}

string Fichin::jugador() const {
    return _jugador;
}

pair<Jugador,Nat> Fichin::objetivo() const {
    auto miPuntaje = ranking()[_jugador];
    pair<Jugador, Nat> res = make_pair(_jugador, miPuntaje);
    for (const Jugador& j : _ranking.claves()) {
        Jugador oponente = j;
        if (ranking()[oponente] < miPuntaje && (res.second < ranking()[oponente] || res.second == miPuntaje))
            res = make_pair(oponente, ranking()[oponente]);
    }

    return res;
}
