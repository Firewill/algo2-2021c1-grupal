#ifndef TIPOS_H
#define TIPOS_H

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <set>
#include <string>
#include <map>
#include <tuple>
#include <list>
#include <vector>

using namespace std;

using Nat = unsigned int;

using Jugador = string;

using Puntaje = Nat;

enum Direccion {
    ARRIBA, ABAJO, IZQUIERDA, DERECHA
};

enum ResultadoMovimiento {
    SIGUE, GANO, PERDIO
};

#endif // TIPOS_H
